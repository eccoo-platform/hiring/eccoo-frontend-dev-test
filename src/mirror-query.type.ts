export type MirrorQueryType = {
  name: string;
  role: string;
};
